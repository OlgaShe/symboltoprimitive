// Object.prototype[Symbol.toPrimitive] = function(hint) {
//     if(hint === "string"){
//         console.log("string");
//         if((typeof(this.toString()) !== "object") && (typeof(this.toString()) !== "function")){
//             return this.toString();;
//         } else if((typeof(this.valueOf()) !== "object") && (typeof(this.valueOf()) !== "function")){
//             return this.valueOf();
//         } else{
//             throw new TypeError();
//         }
//     }else if(hint === "number"){
//         console.log("number");
//         if((typeof(this.valueOf()) !== "object") && (typeof(this.valueOf()) !== "function")){
//             return this.valueOf();;
//         } else if((typeof(this.toString()) !== "object") && (typeof(this.toString()) !== "function")){
//             return this.toString();
//         } else{
//             throw new TypeError();
//         }        
//     }else{
//         console.log("default");
//         return this.toString();
//     }
// }

Object.prototype[Symbol.toPrimitive] = function(hint) {
    let value;
    if(hint === "string"){
        console.log("string");
            value = this.toString();
        if(typeof(value) !== "string"){
            value = this.valueOf();
              if(typeof(value) !== "number"){
                    throw new TypeError();
                }
            return value;
        }
    }else if(hint === "number"){
        console.log("number");
        value = this.valueOf();
        if(typeof(value) !== "number"){
            value = this.toString();
            if(typeof(value) !== "string"){
            throw new TypeError();
        }
            return value;
        }
    }else{
        console.log("default");
        return this.toString();
    }
}

let a = {};

//string
alert(a);
String(a);

//number
+a + 5;
a / 5;
Number(a);

//default
a + 5;
a + [];